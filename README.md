To add this file to your git repo run the following command:

    curl https://bitbucket.org/roadsidemultimedia/wpengine-gitignore/raw/master/gitignore > .gitignore

Make sure you are in the root directory of your git project!
